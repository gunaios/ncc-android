package co.chatsdk.firebase;

import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.ServerValue;

import co.chatsdk.core.utils.StringChecker;
import io.reactivex.Completable;
import timber.log.Timber;

/**
 * Created by ben on 9/11/17.
 */

public class FirebaseEntity {

    public static Completable pushThreadDetailsUpdated (String threadEntityID) {
        return pushUpdated(FirebasePaths.ThreadsPath, threadEntityID, FirebasePaths.DetailsPath,true);
    }

    public static Completable pushThreadUsersUpdated (String threadEntityID) {
        return pushUpdated(FirebasePaths.ThreadsPath, threadEntityID, FirebasePaths.UsersPath,true);
    }

    public static Completable pushThreadMessagesUpdated (String threadEntityID) {
        return pushUpdated(FirebasePaths.ThreadsPath, threadEntityID, FirebasePaths.MessagesPath,true);
    }

    public static Completable pushUserMetaUpdated (String threadEntityID) {
        return pushUpdated(FirebasePaths.UsersPath, threadEntityID, FirebasePaths.MetaPath,false);
    }

    public static Completable pushUserThreadsUpdated (String userEntityID) {
        return pushUpdated(FirebasePaths.UsersPath, userEntityID, FirebasePaths.ThreadsPath,false);
    }

    public static Completable pushUpdated (final String path, final String entityID, final String key,final boolean isThreadPath) {
        return Completable.create(e -> {

            Timber.v(path +'/'+entityID+'/'+key);

            if(StringChecker.isNullOrEmpty(path) || StringChecker.isNullOrEmpty(entityID) || StringChecker.isNullOrEmpty(key)) {
                e.onComplete();
                return;
            }
            DatabaseReference ref;

            String entityIDNew = entityID.replace("%7C","|");

            String pathNew = path.replace("%7C","|");

            String keyNew = key.replace("%7C","|");

            if(isThreadPath){
              ref = FirebasePaths.firebaseChatRootRef().child(pathNew).child(entityIDNew).child(FirebasePaths.UpdatedPath).child(keyNew);
            }else{
                ref = FirebasePaths.usersRef().child(entityIDNew).child(FirebasePaths.UpdatedPath).child(keyNew);
            }

            ref.setValue(ServerValue.TIMESTAMP, (databaseError, databaseReference) -> {
                if(databaseError == null) {
                    e.onComplete();
                }
                else {
                    e.onError(databaseError.toException());
                }
            });
        });
    }

}
