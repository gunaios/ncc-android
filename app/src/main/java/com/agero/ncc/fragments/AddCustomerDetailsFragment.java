package com.agero.ncc.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.model.job.Customer;
import com.agero.ncc.utils.NccConstants;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;
import butterknife.Unbinder;

import static com.agero.ncc.fragments.AddJobsDetailsFragment.newJob;


public class AddCustomerDetailsFragment extends BaseFragment {

    HomeActivity mHomeActivity;
    Unbinder unbinder;
    @BindView(R.id.edit_customer_name)
    EditText editCustomerName;
    @BindView(R.id.edit_phone_number)
    EditText editCustomerPhone;
    @BindView(R.id.spinner_customer_payment)
    Spinner spinnerCustomerPayment;

    public AddCustomerDetailsFragment() {
        // Intentionally empty
    }

    public static AddCustomerDetailsFragment newInstance(boolean isAddNew) {
        AddCustomerDetailsFragment fragment = new AddCustomerDetailsFragment();
        Bundle bundle = new Bundle();
        bundle.putBoolean(NccConstants.BUNDLE_KEY_ADD_NEW, isAddNew);
        fragment.setArguments(bundle);
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_add_customer_details, fragment_content, true);
        unbinder = ButterKnife.bind(this, view);
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_add_customer));
        if (getArguments().getBoolean(NccConstants.BUNDLE_KEY_ADD_NEW)) {

        } else {
            editCustomerName.setText(newJob.getCustomerName());
            editCustomerPhone.setText(newJob.getCustomerCallbackNumber());
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
                    getActivity(), R.array.array_payment, android.R.layout.simple_spinner_item);
            spinnerCustomerPayment.setSelection(adapter.getPosition((CharSequence) newJob.getCustomerPays()));
        }
        return superView;
    }

    @OnClick(R.id.button_add_vehicle_save)
    public void onViewClicked() {
        newJob.setCustomerName(editCustomerName.getText().toString());
        newJob.setCustomerCallbackNumber(editCustomerPhone.getText().toString());
        newJob.setCustomerPays(spinnerCustomerPayment.getSelectedItem().toString());
        if (getArguments().getBoolean(NccConstants.BUNDLE_KEY_ADD_NEW)) {
            mHomeActivity.push(ReviewFragment.newInstance(), getString(R.string.title_review));
        } else {
            mHomeActivity.onBackPressed();
        }
    }
}
