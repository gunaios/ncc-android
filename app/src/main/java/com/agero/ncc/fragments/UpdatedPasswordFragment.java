package com.agero.ncc.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TextInputLayout;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import com.agero.ncc.R;
import com.agero.ncc.activities.HomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.utils.Utils;

import butterknife.BindView;
import butterknife.ButterKnife;


public class UpdatedPasswordFragment extends BaseFragment implements HomeActivity.ToolbarSaveListener, View.OnFocusChangeListener {
    HomeActivity mHomeActivity;
    @BindView(R.id.edit_old_password)
    EditText mEditOldPassword;
    @BindView(R.id.edit_new_password)
    EditText mEditNewPassword;
    @BindView(R.id.edit_confirm_new_password)
    EditText mEditConfirmNewPassword;
    @BindView(R.id.text_input_old_password)
    TextInputLayout mTextInputOldPassword;
    @BindView(R.id.text_input_new_password)
    TextInputLayout mTextInputNewPassword;
    @BindView(R.id.text_new_password_rules)
    TextView mTextNewPasswordRules;
    @BindView(R.id.text_input_confirm_new_password)
    TextInputLayout mTextInputConfirmNewPassword;
    boolean isConfirmPassword,isOldPassword;
    private final TextWatcher mTextWatcher = new TextWatcher() {
        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {

        }

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            enableSave();
        }

        @Override
        public void afterTextChanged(Editable s) {
//            if(mEditOldPassword.isFocused()){
//                validatePasswordField(mTextInputOldPassword, mEditOldPassword, getResources().getString(R.string.password_hint_error));
//            }
//            if(mEditNewPassword.isFocused()){
//                validatePasswordField(mTextInputNewPassword, mEditNewPassword,getResources().getString(R.string.password_hint_error));
//                if (Utils.isValidPassword( mEditNewPassword.getText().toString())) {
//                    mTextNewPasswordRules.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
//                }else{
//                    mTextNewPasswordRules.setTextColor(getResources().getColor(R.color.createacc_error_color));
//                }
//            }
//            if(mEditConfirmNewPassword.isFocused()) {
//                validatePasswordField(mTextInputConfirmNewPassword, mEditConfirmNewPassword, getResources().getString(R.string.password_hint_error));
//
//                if (!mEditConfirmNewPassword.getText().toString().isEmpty() && !mEditNewPassword.getText().toString().equals(mEditConfirmNewPassword.getText().toString())) {
//                    mTextInputConfirmNewPassword.setErrorEnabled(true);
//                    mTextInputConfirmNewPassword.setHintEnabled(true);
//                    mTextInputConfirmNewPassword.setError(getResources().getString(R.string.createacc_error_password_donot_match));
//                } else {
//                    mTextInputConfirmNewPassword.setErrorEnabled(true);
//                    mTextInputConfirmNewPassword.setHintEnabled(true);
//                }
//            }
        }
    };

    public UpdatedPasswordFragment() {
        // Intentionally empty
    }

    public static UpdatedPasswordFragment newInstance() {
        UpdatedPasswordFragment fragment = new UpdatedPasswordFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View superView = super.onCreateView(inflater, container, savedInstanceState);
        ViewGroup fragment_content = superView.findViewById(R.id.fragment_content);

        View view = inflater.inflate(R.layout.fragment_update_password, fragment_content, true);
        ButterKnife.bind(this, view);
        NCCApplication.getContext().getComponent().inject(this);
        mEditor = mPrefs.edit();
        mHomeActivity = (HomeActivity) getActivity();
        mHomeActivity.hideBottomBar();
        mHomeActivity.showToolbar(getString(R.string.title_updated_password));
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
//            Window w = getActivity().getWindow();
//            w.clearFlags(WindowManager.LayoutParams.FLAG_TRANSLUCENT_STATUS);
//        }
        mHomeActivity.setOnToolbarSaveListener(this);
        mEditOldPassword.addTextChangedListener(mTextWatcher);
        mEditNewPassword.addTextChangedListener(mTextWatcher);
        mEditConfirmNewPassword.addTextChangedListener(mTextWatcher);
        mEditOldPassword.setOnFocusChangeListener(this);
        mEditNewPassword.setOnFocusChangeListener(this);
        mEditConfirmNewPassword.setOnFocusChangeListener(this);
        mEditOldPassword.setFocusableInTouchMode(true);
        mEditOldPassword.requestFocus();
        enableSave();
        return superView;
    }

    private void enableSave() {
        if (!mEditNewPassword.getText().toString().trim().isEmpty()
                && !mEditConfirmNewPassword.getText().toString().trim().isEmpty()
                && !mEditOldPassword.getText().toString().isEmpty() && Utils.isValidPassword(mEditNewPassword.getText().toString())) {
            if (!mEditNewPassword.getText().toString().equalsIgnoreCase(mEditConfirmNewPassword.getText().toString())) {
                mHomeActivity.saveDisable();
            } else {
                mHomeActivity.saveEnable();
            }
        } else {
            mHomeActivity.saveDisable();
        }
    }

    @Override
    public void onSave() {
        EditProfileFragment.isPasswordChange = true;
        getActivity().onBackPressed();
    }

    @Override
    public void onFocusChange(View view, boolean hasFocus) {
        if (hasFocus) {
            switch (view.getId()) {
                case R.id.edit_old_password:
                    mEditOldPassword.setHint(getString(R.string.password_hint_old));
                    mTextInputOldPassword.setErrorEnabled(false);
                    if(isOldPassword){
                        validatePasswordField(mTextInputNewPassword, mEditNewPassword,getResources().getString(R.string.password_hint_error));
                    }
                    isPasswordRequired();
                    /*if (!mEditOldPassword.getText().toString().isEmpty()) {
                        mTextInputOldPassword.setErrorEnabled(false);
                    } else {
                        mTextInputOldPassword.setErrorEnabled(true);
                        mTextInputOldPassword.setError(getString(R.string.password_hint_error));
                    }*/
                    break;
                case R.id.edit_new_password:
                    isOldPassword = true;
                    mEditNewPassword.setHint(getString(R.string.password_hint_new));
                    mTextInputNewPassword.setErrorEnabled(false);
                    isPasswordRequired();
                    validatePasswordField(mTextInputOldPassword, mEditOldPassword, getResources().getString(R.string.password_hint_error));
                    break;
                case R.id.edit_confirm_new_password:
                    isConfirmPassword = true;
                    mEditConfirmNewPassword.setHint(getString(R.string.password_hint_confirm_new));
                    mTextInputConfirmNewPassword.setErrorEnabled(false);
                    validatePasswordField(mTextInputOldPassword, mEditOldPassword, getResources().getString(R.string.password_hint_error));
                    validatePasswordField(mTextInputNewPassword, mEditNewPassword,getResources().getString(R.string.password_hint_error));
                    if (Utils.isValidPassword(mEditNewPassword.getText().toString())) {
                        mTextNewPasswordRules.setTextColor(getResources().getColor(R.color.ncc_text_disabled_color));
                    } else {
                        mTextNewPasswordRules.setTextColor(getResources().getColor(R.color.createacc_error_color));
                    }
                    break;
            }
        } else {
            mEditOldPassword.setHint("");
            mEditNewPassword.setHint("");
            mEditConfirmNewPassword.setHint("");
        }
    }

    private void isPasswordRequired(){
        if(isConfirmPassword){
            if (mEditConfirmNewPassword.getText().toString().isEmpty()) {
                validatePasswordField(mTextInputConfirmNewPassword, mEditConfirmNewPassword,getResources().getString(R.string.password_hint_error));
                mTextInputConfirmNewPassword.setErrorTextAppearance(R.style.error_style);
            }else if (!mEditConfirmNewPassword.getText().toString().isEmpty() && !mEditNewPassword.getText().toString().equals(mEditConfirmNewPassword.getText().toString())) {
                mTextInputConfirmNewPassword.setErrorEnabled(true);
                mTextInputConfirmNewPassword.setError(getResources().getString(R.string.createacc_error_password_donot_match));
                mTextInputConfirmNewPassword.setErrorTextAppearance(R.style.error_style);
            } else {
                mTextInputConfirmNewPassword.setErrorEnabled(true);
                mTextInputConfirmNewPassword.setHintEnabled(true);
            }
        }
    }

    private void validatePasswordField(TextInputLayout textField, EditText editText, String validateFields) {
        if (!editText.getText().toString().isEmpty()) {
            textField.setErrorEnabled(false);
        } else {
            textField.setErrorEnabled(true);
            textField.setError(validateFields);
        }
    }
}
