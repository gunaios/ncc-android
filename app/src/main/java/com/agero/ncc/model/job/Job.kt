package com.agero.ncc.model.job

data class Job(

        var serviceType: String? = null,
        var towDestination: TowDestination? = null,
        var currentStatus: CurrentStatus? = null,
        var crossReference: CrossReference? = null,
        var equipment: Equipment? = null,
        var vendorId: String? = null,
        var disablementLocation: DisablementLocation? = null,
        var assignedDriver: AssignedDriver? = null,
        var dispatchRequestNumber: String? = null,
        var equipmentList: List<EquipmentList>? = null,
        var vehicle: Vehicle? = null,
        var responseTimeExtCount: Int? = null,
        var statusHistory: HashMap<String,StatusHistory>? = null,
        var ETA: ETA? = null,
        var acceptedTime: String? = null,
        var driverId: Int? = null,
        var referenceNumber: String? = null,
        var indexDate: String? = null,
        var service: Service? = null,
        var receivedTime: String? = null,
        var make: String? = null,
        var poNumber: String? = null,
        var status: String? = null,
        var customer: Customer? = null


)

data class AssignedDriver(

        var serviceType: String? = null,
        var gPSTracking: Boolean? = null,
        var smartPhone: Boolean? = null,
        var equipment: Equipment? = null,
        var disablementLocation: DisablementLocation? = null,
        var officeEmail: String? = null,
        var onDuty: Boolean? = null,
        var currentLocation: CurrentLocation? = null,
        var driverId: Int? = null,
        var officePhone: String? = null,
        var profileLastUpdateDateTime: String? = null,
        var name: String? = null,
        var autoAssignEnabled: Boolean? = null,
        var cellPhone: String? = null,
        var username: String? = null
)

data class ContactInfo(

        var phone: String? = null,
        var name: String? = null

)

data class CrossReference(

        var dispatchVendorStatId: String? = null,
        var clientId: String? = null,
        var caseId: String? = null,
        var isVinCaptureRequired: Boolean? = null,
        var prodId: Int? = null

)

data class CurrentLocation(

        var latitude: Int? = null,
        var longitude: Int? = null

)

data class CurrentStatus(

        var dispatcherLogin: String? = null,
        var dispatcherName: String? = null,
        var code: String? = null,
        var assignedDriverLogin: String? = null,
        var latitude: Int? = null,
        var assignedDriverId: String? = null,
        var description: String? = null,
        var assignedDriverName: String? = null,
        var source: String? = null,
        var extJobId: String? = null,
        var reportedBy: String? = null,
        var eta: String? = null,
        var driverId: Int? = null,
        var jobModifiedDateTime: String? = null,
        var statusTime: String? = null,
        var reasonCode: Int? = null,
        var dispatcherId: String? = null,
        var dispatchAcceptedDateTime: String? = null,
        var jobOfferedDateTime: String? = null,
        var longitude: Int? = null

)

data class DisablementLocation(

        var parked: Boolean? = null,
        var contactInfo: ContactInfo? = null,
        var driverWithVehicle: Boolean? = null,
        var location: Location? = null,
        var nightDropOff: Boolean? = null

)

data class Equipment(

        var type: String? = null,
        var equipmentId: String? = null,
        var addressId: Int? = null

)

data class EquipmentList(

        var type: String? = null,
        var equipmentId: String? = null,
        var addressId: Int? = null

)

data class ETA(

        var etaRevisionCount: Int? = null,
        var eTA: String? = null,
        var etaInMinutes: Int? = null,
        var etaStatusCode: Int? = null,
        var revisedEta: String? = null,
        var revisedEtaInMinutes: Int? = null

)

data class Location(

        var city: String? = null,
        var address1: String? = null,
        var latitude: Float? = null,
        var postalCode: String? = null,
        var locationType: String? = null,
        var state: String? = null,
        var longitude: Float? = null

)

data class Service(

        var coverage: String? = null,
        var serviceType: String? = null,
        var cancelPending: Boolean? = null,
        var problem: String? = null,
        var urgency: String? = null,
        var dispatchSource: String? = null,
        var stateFarmJob: Boolean? = null,
        var refJob: Boolean? = null

)

data class StatusHistory(

        var dispatcherName: String? = null,
        var code: String? = null,
        var latitude: Int? = null,
        var description: String? = null,
        var assignedDriverName: String? = null,
        var source: String? = null,
        var extJobId: String? = null,
        var reportedBy: String? = null,
        var eta: String? = null,
        var driverId: Int? = null,
        var jobModifiedDateTime: String? = null,
        var statusTime: String? = null,
        var reasonCode: Int? = null,
        var dispatcherId: String? = null,
        var dispatchAcceptedDateTime: String? = null,
        var jobOfferedDateTime: String? = null,
        var longitude: Int? = null,
        var statusChangedBy: String? = null,
        var role: String? = null

)

data class TowDestination(

        var contactInfo: ContactInfo? = null,
        var location: Location? = null,
        var nightDropOff: Boolean? = null

)

data class Vehicle(

        var driveTrainTypeCode: String? = null,
        var color: String? = null,
        var year: String? = null,
        var driveTrainType: String? = null,
        var fuelType: String? = null,
        var fuelTypeCode: String? = null,
        var vehicleTypeCode: String? = null,
        var model: String? = null,
        var vin: String? = null,
        var plate: String? = null,
        var id: Int? = null,
        var make: String? = null,
        var vehicleType: String? = null,
        var mileage: Int? = null

)

data class Customer(
        var name: String?=null,
        var paymentMethod: String?=null,
        var coverage: String?=null,
        var payment: Int?=null
)