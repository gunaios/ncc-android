package com.agero.ncc.app

import android.content.Context
import android.content.SharedPreferences

import com.agero.ncc.retrofit.NccApi

import javax.inject.Singleton

import dagger.Module
import dagger.Provides
import retrofit2.Retrofit

import android.content.Context.MODE_PRIVATE

@Module
class AppModule(private val mApp: NCCApplication) {
    companion object {
        const val NCC_PREFS = "NccPrefs"
    }

    @Provides
    @Singleton
    fun provideApplicationContext(): Context {
        return mApp
    }

    @Provides
    @Singleton
    fun provideSharedPreferences(): SharedPreferences {
        return mApp.getSharedPreferences(NCC_PREFS, MODE_PRIVATE)
    }

    @Provides
    fun provideNccApiInterface(retrofit: Retrofit): NccApi {
        return retrofit.create(NccApi::class.java)
    }
}
