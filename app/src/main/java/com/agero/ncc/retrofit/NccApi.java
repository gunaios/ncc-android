package com.agero.ncc.retrofit;


import com.agero.ncc.model.CreateUserRequest;
import com.agero.ncc.model.CreateUserResponse;
import com.agero.ncc.model.CustomerInfoModel;
import com.agero.ncc.model.ForgotPasswordRequest;
import com.agero.ncc.model.Profile;
import com.agero.ncc.model.RefreshTokenRequest;
import com.agero.ncc.model.SigninRequest;
import com.agero.ncc.model.SigninResponseModel;
import com.agero.ncc.model.TermsAndConditionsAcceptenceModel;
import com.agero.ncc.model.TermsAndConditionsModel;
import com.agero.ncc.model.TowDestinationsModel;
import com.agero.ncc.model.TowDestinationsRequest;
import com.agero.ncc.model.VerifyUser;
import com.agero.ncc.model.job.TowDestination;
import com.agero.ncc.model.vehicledetails.MyVehicleDetails;

import org.json.JSONObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.PUT;
import retrofit2.http.Url;

public interface NccApi {

    @GET
    Call<MyVehicleDetails> getVehicleDetails(@Url String url, @Header("authorization") String auth);

    @GET
    Call<VerifyUser> isUSerExists(@Url String url, @Header("authorization") String auth);

    @POST
    Call<SigninResponseModel> Signin(@Url String url, @Header("authorization") String auth, @Body SigninRequest body);

    @POST
    Call<CreateUserResponse> createUser(@Url String url, @Header("authorization") String auth, @Body CreateUserRequest body);

    @POST
    Call<SigninResponseModel> refreshToken(@Url String url, @Header("authorization") String auth, @Body RefreshTokenRequest body);

    @GET
    Call<TermsAndConditionsModel> getTermsAndConditions(@Url String url, @Header("authorization") String auth);

    @GET
    Call<TermsAndConditionsAcceptenceModel> getTermsAndConditionsAcepetence(@Url String url, @Header("authorization") String auth);

    @POST
    Call<TermsAndConditionsAcceptenceModel> postTermsAndConditionsAcepetence(@Url String url, @Header("authorization") String auth);

    @PUT
    Call<Object> putProfile(@Url String url, @Header("authorization") String auth, @Body Profile body);

    @GET
    Call<CustomerInfoModel> getCustomerInfo(@Url String url, @Header("authorization") String auth);

    @POST
    Call<ResponseBody> forgotPassword(@Url String url, @Header("authorization") String auth, @Body ForgotPasswordRequest body);

    @POST
    Call<TowDestinationsModel> getTowDestinations(@Url String url, @Header("x-apikey") String apikey,@Header("Content-Type") String contentType,@Header("x-agero-programcode") String ageroPromocode, @Body TowDestinationsRequest body);

}
