package com.agero.ncc.services;

import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.Service;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.SharedPreferences;
import android.location.Location;
import android.os.Build;
import android.os.Handler;
import android.os.IBinder;
import android.os.Looper;
import android.provider.Settings;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.support.v4.app.NotificationCompat;
import android.support.v4.content.LocalBroadcastManager;
import android.text.TextUtils;
import android.util.Log;
import android.widget.Toast;

import com.agero.ncc.BuildConfig;
import com.agero.ncc.activities.WelcomeActivity;
import com.agero.ncc.app.NCCApplication;
import com.agero.ncc.firebasefunctions.FirebaseFunctions;
import com.agero.ncc.firebasefunctions.FirebaseFunctionsException;
import com.agero.ncc.firebasefunctions.HttpsCallableResult;
import com.agero.ncc.model.CombinedData;
import com.agero.ncc.model.JobDetail;
import com.agero.ncc.model.SparkLocationData;
import com.agero.ncc.model.Status;
import com.agero.ncc.utils.DateTimeUtils;
import com.agero.ncc.utils.NccConstants;
import com.agero.ncc.utils.TokenManager;
import com.agero.ncc.utils.UserError;
import com.agero.ncc.utils.Utils;
import com.agero.nccsdk.NccException;
import com.agero.nccsdk.NccSdk;
import com.agero.nccsdk.NccSensorListener;
import com.agero.nccsdk.NccSensorType;
import com.agero.nccsdk.adt.event.AdtEventListener;
import com.agero.nccsdk.domain.data.NccLocationData;
import com.agero.nccsdk.domain.data.NccMotionData;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.gson.Gson;
import com.splunk.mint.Mint;
import com.splunk.mint.MintLogLevel;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.annotations.NonNull;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.observers.DisposableObserver;
import io.reactivex.schedulers.Schedulers;
import timber.log.Timber;

/**
 * Created by sdevabhaktuni on 1/26/18.
 */

public final class SparkLocationUpdateService extends Service {

    private NccSensorListener motionSensorListener;
    private NccSensorListener locationSensorListener;
    private DatabaseReference myRef;
    private DatabaseReference myLocationRef;
    private  AdtEventListener adtEventListener;
    private volatile NccMotionData mMotionData = null;

    SharedPreferences mPrefs;
    public static boolean onDutyFlag = false;
    public static final float GEOFENCE_DISTANCE = 300f;

//    public static ArrayList<JobDetail> jobDataForGeofenceArrayList = new ArrayList();
    public static CopyOnWriteArrayList<JobDetail> jobDataForGeofenceArrayList = new CopyOnWriteArrayList();
    private static ArrayList<String> updateHappeningForJob = new ArrayList<>();
    private static HashMap<String, Long> onSceneTime = new HashMap<>();
    private HashMap<String, Long> mMatchingCount = new HashMap<>();
    private static final int MATCHES_TIME_DIFFERENCE = 90000;

    private HashMap<String, Integer> mRetryCount = new HashMap<>();
    private static final int MAX_RETRY_COUNT = 3;
    private CompositeDisposable disposables = new CompositeDisposable();

    @Override
    public void onCreate() {

        if (Build.VERSION.SDK_INT >= 26) {
            startForegroundNotification();
        }

        NccSdk.getNccSdk(getApplication()).startTracking();


        if(adtEventListener == null) {
            adtEventListener = new AdtEventListener() {
                @Override
                public void onDrivingStart() {
                    Log.d("LocationUpdateService", "Driving started!");
                    subscribeLocation();
                    Mint.logEvent("Driving started");
                    Mint.flush();
                }

                @Override
                public void onDrivingStop() {
                    Log.d("LocationUpdateService", "Driving stopped!");
                    unSubscribeLocation();
                    Mint.logEvent("Driving stopped");
                    Mint.flush();
                }

            };
        }

        NccSdk.getNccSdk(getApplication()).addDrivingListener(adtEventListener);

    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    private void startForegroundNotification(){
        String CHANNEL_ID = "SparkLocationChanel";
        NotificationChannel channel = new NotificationChannel(CHANNEL_ID,
                "Spark Location Service",
                NotificationManager.IMPORTANCE_LOW);

        ((NotificationManager) getSystemService(Context.NOTIFICATION_SERVICE)).createNotificationChannel(channel);

        Notification notification = new NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle("Spark Location Service")
                .setContentText("").build();

        startForeground(1, notification);
    }


    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        super.onStartCommand(intent, flags, startId);

        mPrefs = this.getSharedPreferences("NccPrefs", MODE_PRIVATE);

        if (Build.VERSION.SDK_INT >= 26) {
            startForegroundNotification();
        }

        if (startId == 1) {
            FirebaseDatabase database = FirebaseDatabase.getInstance();
            myRef = database.getReference("Users/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                    + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
            myLocationRef = database.getReference("Locations/" + mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, "")
                    + "/" + mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));

        }

        return START_STICKY;
    }


    public SparkLocationUpdateService() {
        super();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();

        NccSdk.getNccSdk(getApplication()).stopTracking();

        Timber.i("ondestroy!");
        if (onDutyFlag) {
            Mint.logEvent("Starting SparkLocationService from onDestroy");
            Intent broadcastIntent = new Intent("com.agero.ncc.START_SERVICE");
            sendBroadcast(broadcastIntent);
        } else {
            Mint.logEvent("SparkLocationService is Stopped, because onDutyFlag: " + onDutyFlag);
        }
    }

    public static boolean isRunning(Context context) {
        final ActivityManager activityManager = (ActivityManager) context.getSystemService(Context.ACTIVITY_SERVICE);
        final List<ActivityManager.RunningServiceInfo> services = activityManager.getRunningServices(Integer.MAX_VALUE);

        for (ActivityManager.RunningServiceInfo runningServiceInfo : services) {
            //Log.d(Constants.TAG, String.format("Service:%s", runningServiceInfo.service.getClassName()));
            if (runningServiceInfo.service.getClassName().equals(SparkLocationUpdateService.class.getName())) {
                return true;
            }
        }
        return false;
    }


    private void subscribeLocation() {

        if (disposables.size() == 0) {
            String deviceId = TokenManager.getInstance().getDeviceId();

            // Init observable
            Observable<NccLocationData> locationObservable = Observable.create(emitter -> {
                locationSensorListener = (sensorType, data) -> {
                    if (sensorType == NccSensorType.LOCATION) {
                        emitter.onNext((NccLocationData) data);
                    }
                };

                NccSdk.getNccSdk(getApplication()).addTrackingListener(locationSensorListener);


            });

            Observable<NccMotionData> motionObservable = Observable.create(emitter -> {
                motionSensorListener = (sensorType, data) -> {
                    if (sensorType == NccSensorType.MOTION_ACTIVITY) {
                        emitter.onNext((NccMotionData) data);
                    }
                };
                NccSdk.getNccSdk(getApplication()).addTrackingListener(motionSensorListener);
            });


            // cache the result of the observable.
            // Observable<NccSensorData> cacheObservable = observable.cache();

            DisposableObserver<NccLocationData> firebaseUpdateobserver = new DisposableObserver<NccLocationData>() {
                @Override
                public void onNext(@NonNull NccLocationData location) {

                    checkCurrentJobGeofence(location);

                    try {

                        SparkLocationData locationData = new SparkLocationData(
                                location.getTimestamp(),
                                location.getLatitude(),
                                location.getLongitude(),
                                location.getAltitude(),
                                location.getCourse(),
                                location.getAccuracy(),
                                location.getSpeed(),
                                location.getDistance(),
                                "Android",
                                deviceId
                        );


                        Log.d("SparkLocationUpdate", "Location: " + location.getLatitude() + ": " + location.getLongitude());
                        // Do something with each location
                        if (myRef != null) {
                            myRef.child("location").setValue(locationData);
                            NCCApplication.mLocationData = locationData;
                        }
                        if (myLocationRef != null) {
                            myLocationRef.push().setValue(locationData);
                        }
                    }catch (Exception e){}


                }

                @Override
                public void onError(@NonNull Throwable e) {
                    // TODO Handle exception
                }

                @Override
                public void onComplete() {

                }
            };


            // cache the result of the observable.
            // Observable<NccSensorData> cacheObservable = observable.cache();

            DisposableObserver<NccMotionData> motionObserver = new DisposableObserver<NccMotionData>() {
                @Override
                public void onNext(@NonNull NccMotionData data) {

                    Log.d("SparkLocationUpdate", "ActivityString: " + data.getActivityString() + " : Confidence" + data.getConfidence());
                    // Do something with each location
                    mMotionData = data;
                }

                @Override
                public void onError(@NonNull Throwable e) {
                    // TODO Handle exception
                }

                @Override
                public void onComplete() {

                }
            };


            // Subscribe to observable
            disposables.add(locationObservable
                    .sample(4, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(firebaseUpdateobserver)
            );


            disposables.add(motionObservable
                    .sample(4, TimeUnit.SECONDS)
                    .subscribeOn(Schedulers.io())
                    .subscribeWith(motionObserver)
            );

        }
    }


    private void unSubscribeLocation() {
        try {
            if(locationSensorListener != null) {
                NccSdk.getNccSdk(getApplication()).removeTrackingListener(locationSensorListener);
            }
            if(motionSensorListener != null) {
                NccSdk.getNccSdk(getApplication()).removeTrackingListener(motionSensorListener);
            }
            if (disposables != null && disposables.size() > 0) {
                disposables.dispose();
                disposables.clear();
                disposables = null;
            }
            disposables = new CompositeDisposable();
            //noOfTimesMatched = 0;
            mMatchingCount.clear();
            mRetryCount.clear();
        } catch (NccException ne) {

        }
    }



    private void checkCurrentJobGeofence(NccLocationData data) {

        if (jobDataForGeofenceArrayList != null && jobDataForGeofenceArrayList.size() > 0) {

            CopyOnWriteArrayList<JobDetail> tempjobDataForGeofenceArrayList = (CopyOnWriteArrayList<JobDetail>) jobDataForGeofenceArrayList.clone();

            if(tempjobDataForGeofenceArrayList != null && tempjobDataForGeofenceArrayList.size() >0) {
                for (JobDetail currentjob : tempjobDataForGeofenceArrayList) {

                    if (currentjob != null && currentjob.getCurrentStatus() != null && !TextUtils.isEmpty(currentjob.getCurrentStatus().getStatusCode())) {
                        if (NccConstants.JOB_STATUS_EN_ROUTE.equalsIgnoreCase(currentjob.getCurrentStatus().getStatusCode())) {
                            Location disablementLocation = new Location("DisablementLocation");
                            disablementLocation.setLatitude(currentjob.getDisablementLocation().getLatitude());
                            disablementLocation.setLongitude(currentjob.getDisablementLocation().getLongitude());
                            com.agero.ncc.model.Location currentLocation = new com.agero.ncc.model.Location(data.getLatitude(), data.getLongitude());
                            float distance = calculateDistance(currentLocation, disablementLocation);

                            long lastTimeMatched = mMatchingCount.containsKey(currentjob.getDispatchId()) ? mMatchingCount.get(currentjob.getDispatchId()) : 0;
                            if (BuildConfig.DEBUG) {
                                HashMap<String, String> extraGeofenceData = new HashMap<>();
                                extraGeofenceData.put("job_id", currentjob.getDispatchId());
                                extraGeofenceData.put("distance", "" + distance);
                                extraGeofenceData.put("noOfTimesMatched", "" + lastTimeMatched);

                                if (mMotionData != null) {
                                    extraGeofenceData.put("LastMotionData", "" + mMotionData.getActivityString());
                                    extraGeofenceData.put("Confidence", "" + mMotionData.getConfidence());
                                }

                                mintlogEventExtraData("Geofence-Onscene:" + currentjob.getDispatchId(), extraGeofenceData);
                                Mint.flush();
                            }

                            if (distance != 0f && distance <= GEOFENCE_DISTANCE) {

                                if(lastTimeMatched == 0) {
                                    mMatchingCount.put(currentjob.getDispatchId(), data.getTimestamp());
                                }
                                int retryCount = mRetryCount.containsKey(currentjob.getDispatchId()) ? mRetryCount.get(currentjob.getDispatchId()) : 0;
                                if (retryCount == 0) {
                                    if (lastTimeMatched !=0 && ((data.getTimestamp()-lastTimeMatched) >= MATCHES_TIME_DIFFERENCE) || (mMotionData != null && ("on_foot".equalsIgnoreCase(mMotionData.getActivityString()) || "walking".equalsIgnoreCase(mMotionData.getActivityString())) && mMotionData.getConfidence() >= 50)) {
                                        Long enterTime = Calendar.getInstance().getTimeInMillis();
                                        HashMap<String, String> extraDatas = new HashMap<>();
                                        extraDatas.put("dispatch_id", currentjob.getDispatchId());
                                        extraDatas.put("time", "" + enterTime);
                                        mintlogEventExtraData("Disablement Location Enter", extraDatas);
                                        Timber.d("Disablement Location Enter dispatch_id =" + currentjob.getDispatchId() + " time = " + enterTime);
                                        if (onSceneTime != null && !onSceneTime.containsKey(currentjob.getDispatchId())) {
                                            onSceneTime.put(currentjob.getDispatchId(), enterTime);
                                        }
                                        updateStatus(currentjob, NccConstants.JOB_STATUS_ON_SCENE, mMotionData);
                                    }
                                }
                            }
                        } else if (NccConstants.JOB_STATUS_ON_SCENE.equalsIgnoreCase(currentjob.getCurrentStatus().getStatusCode())) {
                            Location disablementLocation = new Location("DisablementLocation");
                            disablementLocation.setLatitude(currentjob.getDisablementLocation().getLatitude());
                            disablementLocation.setLongitude(currentjob.getDisablementLocation().getLongitude());
                            com.agero.ncc.model.Location currentLocation = new com.agero.ncc.model.Location(data.getLatitude(), data.getLongitude());
                            float distance = calculateDistance(currentLocation, disablementLocation);

                            long lastTimeMatched = mMatchingCount.containsKey(currentjob.getDispatchId()) ? mMatchingCount.get(currentjob.getDispatchId()) : 0;
                            if (BuildConfig.DEBUG) {
                                HashMap<String, String> extraGeofenceData = new HashMap<>();
                                extraGeofenceData.put("job_id", currentjob.getDispatchId());
                                extraGeofenceData.put("distance", "" + distance);
                                extraGeofenceData.put("noOfTimesMatched", "" + lastTimeMatched);

                                if (mMotionData != null) {
                                    extraGeofenceData.put("LastMotionData", "" + mMotionData.getActivityString());
                                    extraGeofenceData.put("Confidence", "" + mMotionData.getConfidence());
                                }

                                mintlogEventExtraData("Geofence-TowInProgress:" + currentjob.getDispatchId(), extraGeofenceData);
                                Mint.flush();
                            }


                            if (distance != 0f && distance > GEOFENCE_DISTANCE) {

                                if(lastTimeMatched == 0) {
                                    mMatchingCount.put(currentjob.getDispatchId(), data.getTimestamp());
                                }
                                int retryCount = mRetryCount.containsKey(currentjob.getDispatchId()) ? mRetryCount.get(currentjob.getDispatchId()) : 0;
                                if (retryCount == 0) {
                                    if (lastTimeMatched !=0 && ((data.getTimestamp()-lastTimeMatched) >= 15000) || (mMotionData != null && ("in_vehicle".equalsIgnoreCase(mMotionData.getActivityString()) && mMotionData.getConfidence() >= 50))) {
                                        Long exitTime = Calendar.getInstance().getTimeInMillis();
                                        HashMap<String, String> extraDatasExit = new HashMap<>();
                                        extraDatasExit.put("dispatch_id", currentjob.getDispatchId());
                                        extraDatasExit.put("time", "" + exitTime);
                                        mintlogEventExtraData("Disablement Location Exit", extraDatasExit);
                                        Timber.d("Disablement Location Exit dispatch_id =" + currentjob.getDispatchId() + " time = " + exitTime);

                                        if (onSceneTime != null && onSceneTime.containsKey(currentjob.getDispatchId())) {
                                            Long enterTime = onSceneTime.get(currentjob.getDispatchId());
                                            onSceneTime.remove(currentjob.getDispatchId());
                                            Long onSceneTimeDiff = exitTime - enterTime;
                                            HashMap<String, String> extraDatasOnScene = new HashMap<>();
                                            extraDatasOnScene.put("dispatch_id", currentjob.getDispatchId());
                                            extraDatasOnScene.put("time", "" + onSceneTimeDiff);
                                            mintlogEventExtraData("Disablement Location Duration", extraDatasOnScene);
                                            Timber.d("Disablement Location Duration dispatch_id =" + currentjob.getDispatchId() + " time = " + onSceneTimeDiff);
                                        }
                                        updateStatus(currentjob, NccConstants.JOB_STATUS_TOWING, mMotionData);
                                    }
                                }
                            }

                        } else if (NccConstants.JOB_STATUS_TOWING.equalsIgnoreCase(currentjob.getCurrentStatus().getStatusCode()) && currentjob.getTowLocation() != null) {

                            Location towLocation = new Location("TowLocation");
                            towLocation.setLatitude(currentjob.getTowLocation().getLatitude());
                            towLocation.setLongitude(currentjob.getTowLocation().getLongitude());
                            com.agero.ncc.model.Location currentLocation = new com.agero.ncc.model.Location(data.getLatitude(), data.getLongitude());
                            float distance = calculateDistance(currentLocation, towLocation);

                            long lastTimeMatched = mMatchingCount.containsKey(currentjob.getDispatchId()) ? mMatchingCount.get(currentjob.getDispatchId()) : 0;
                            if (BuildConfig.DEBUG) {
                                HashMap<String, String> extraGeofenceData = new HashMap<>();
                                extraGeofenceData.put("job_id", currentjob.getDispatchId());
                                extraGeofenceData.put("distance", "" + distance);
                                extraGeofenceData.put("noOfTimesMatched", "" + lastTimeMatched);

                                if (mMotionData != null) {
                                    extraGeofenceData.put("LastMotionData", "" + mMotionData.getActivityString());
                                    extraGeofenceData.put("Confidence", "" + mMotionData.getConfidence());
                                }



                                mintlogEventExtraData("Geofence-TowArrived:" + currentjob.getDispatchId(), extraGeofenceData);
                                Mint.flush();
                            }
                            if (distance != 0f && distance < GEOFENCE_DISTANCE) {

                                if(lastTimeMatched == 0) {
                                    mMatchingCount.put(currentjob.getDispatchId(), data.getTimestamp());
                                }
                                int retryCount = mRetryCount.containsKey(currentjob.getDispatchId()) ? mRetryCount.get(currentjob.getDispatchId()) : 0;
                                if (retryCount == 0) {
                                    if (lastTimeMatched !=0 && ((data.getTimestamp()-lastTimeMatched) >= MATCHES_TIME_DIFFERENCE) || (mMotionData != null && ("on_foot".equalsIgnoreCase(mMotionData.getActivityString()) || "walking".equalsIgnoreCase(mMotionData.getActivityString())) && mMotionData.getConfidence() >= 50)) {
                                        updateStatus(currentjob, NccConstants.JOB_STATUS_DESTINATION_ARRIVAL, mMotionData);
                                    }
                                }

                            } else if (NccConstants.JOB_STATUS_DESTINATION_ARRIVAL.equalsIgnoreCase(currentjob.getCurrentStatus().getStatusCode())) {

                                jobDataForGeofenceArrayList.remove(currentjob);
                            }
                        }
                    }
                }
            } else {
                mintlogEvent("geofence list is empty, Array copy error ");
                Mint.flush();
                // send log that geofence list is empty
            }
        } else {
            mintlogEvent("geofence list is empty ");
            Mint.flush();
            // send log that geofence list is empty
        }

    }

    public static float calculateDistance(com.agero.ncc.model.Location userCurrentLocation , Location destination) {

        float distance = 0f;
        try {

            Location currentLocation = new Location("currentLocation");

            currentLocation.setLatitude(userCurrentLocation.getLatitude());
            currentLocation.setLongitude(userCurrentLocation.getLongitude());

            distance = currentLocation.distanceTo(destination);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return distance;
    }


    private void updateStatus(JobDetail currentjob, String statusCode, NccMotionData data) {
        if (!updateHappeningForJob.contains(currentjob.getDispatchId())) {
            updateHappeningForJob.add(currentjob.getDispatchId());
            int retryCount = mRetryCount.containsKey(currentjob.getDispatchId()) ? mRetryCount.get(currentjob.getDispatchId()) : 0;
            retryCount++;
            mRetryCount.put(currentjob.getDispatchId(), retryCount);
            TokenManager.getInstance().validateToken(this, new TokenManager.TokenReponseListener() {
                @Override
                public void onRefreshSuccess() {
                    HashMap<String, Object> statusupdateObj = new HashMap<String, Object>();

                    statusupdateObj.put("serverTimeUtc", DateTimeUtils.getUtcTimeFormat());
                    statusupdateObj.put("statusCode", statusCode);
                    statusupdateObj.put("dispatchId", currentjob.getDispatchId());
                    statusupdateObj.put("isPending", false);
                    statusupdateObj.put("userName", mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                    statusupdateObj.put("userId", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                    statusupdateObj.put("authToken", mPrefs.getString(NccConstants.APIGEE_TOKEN, ""));
                    statusupdateObj.put("statusTime", DateTimeUtils.getUtcTimeFormat());
                    statusupdateObj.put("source", NccConstants.SOURCE);
                    statusupdateObj.put("subSource", NccConstants.SUB_SOURCE);
                    statusupdateObj.put("deviceId", Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

                    statusupdateObj.put("dispatchAssignedToId", currentjob.getDispatchAssignedToId());
                    statusupdateObj.put("dispatchAssignedToName", currentjob.getDispatchAssignedToName());
                    statusupdateObj.put("driverAssignedById", currentjob.getDriverAssignedById());
                    statusupdateObj.put("driverAssignedByName", currentjob.getDriverAssignedByName());
                    statusupdateObj.put("isGeofence", true);


                    //add the logs to mint

                    HashMap<String, String> extraDatas = new HashMap<>();
                    extraDatas.put("current_status", new Gson().toJson(statusupdateObj));
                    extraDatas.put("dispatch_id", currentjob.getDispatchId());
                    if (data != null) {
                        extraDatas.put("activity", data.getActivityString());
                        extraDatas.put("activity-confidence", "" + data.getConfidence());
                    }
                    mintlogEventExtraData("GeoFence - Update Status", extraDatas);


                    statusupdateObj.put("statusHistory", new Gson().toJson(currentjob.getStatusHistory()));

                    FirebaseFunctions.getInstance()
                            .getHttpsCallable(NccConstants.API_JOB_STATUS_UPDATE)
                            .call(statusupdateObj)
                            .addOnFailureListener(new OnFailureListener() {

                                @Override
                                public void onFailure(@android.support.annotation.NonNull Exception e) {

                                    Timber.d("failed");
                                    updateHappeningForJob.remove(currentjob.getDispatchId());
                                    UserError mUserError = new UserError();
                                    if (e instanceof FirebaseFunctionsException) {
                                        FirebaseFunctionsException ffe = (FirebaseFunctionsException) e;
                                        FirebaseFunctionsException.Code code = ffe.getCode();
                                        Object details = ffe.getDetails();
                                        mintlogEvent("Spark Location Service updateStatus: " + statusCode + " Firebase Functions Error - Error Message -" + ffe.getMessage());

                                    } else if (e.getMessage() != null) {

                                        mintlogEvent("Spark Location Service updateStatus: " + statusCode + " Firebase Functions Error - Error Message -" + e.getMessage());
                                    } else {
                                        mintlogEvent("Spark Location Service updateStatus Firebase Functions Error - Error Message - failed");
                                    }

                                    int retryCountAfterFailure = mRetryCount.containsKey(currentjob.getDispatchId()) ? mRetryCount.get(currentjob.getDispatchId()) : 0;
                                    if (retryCountAfterFailure <= MAX_RETRY_COUNT) {
                                        updateStatus(currentjob, statusCode, data);
                                    } else {
                                        //retryCount = 0;
                                        mRetryCount.remove(currentjob.getDispatchId());
                                        //noOfTimesMatched = 0;
                                        mMatchingCount.remove(currentjob.getDispatchId());
                                        removeJobDataForGeofenceArrayQueue(currentjob.getDispatchId());
                                    }
                                }
                            })
                            .addOnSuccessListener(new OnSuccessListener<HttpsCallableResult>() {
                                @Override
                                public void onSuccess(HttpsCallableResult httpsCallableResult) {
                                    //update the job in the jobDataForGeofenceArrayQueue

                                    if (currentjob.getCurrentStatus() != null) {

                                        Status currentStatus = new Status();
                                        currentStatus.setStatusCode(statusCode);
                                        currentStatus.setUserId(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
                                        currentStatus.setUserName(mPrefs.getString(NccConstants.SIGNIN_USER_NAME, ""));
                                        currentStatus.setStatusTime(DateTimeUtils.getUtcTimeFormat());
                                        currentStatus.setServerTimeUtc(DateTimeUtils.getUtcTimeFormat());
                                        currentStatus.setSource(NccConstants.SOURCE);
                                        currentStatus.setSubSource(NccConstants.SUB_SOURCE);
                                        currentStatus.setDeviceId(Settings.Secure.getString(NCCApplication.getContext().getContentResolver(), Settings.Secure.ANDROID_ID));

                                        currentjob.setCurrentStatus(currentStatus);
                                        updateHappeningForJob.remove(currentjob.getDispatchId());
                                        if (NccConstants.IS_GEOFENCE_ENABLED) {
                                            checkJobToAddOrRemove(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""), currentjob);
                                        }

                                        //retryCount = 0;
                                        mRetryCount.remove(currentjob.getDispatchId());
                                        //noOfTimesMatched = 0;
                                        mMatchingCount.remove(currentjob.getDispatchId());
                                    }


                                }
                            });
                }

                @Override
                public void onRefreshFailure() {
                    updateHappeningForJob.remove(currentjob.getDispatchId());
                    tokenRefreshFailed();

                }
            });
        }
    }


    private void tokenRefreshFailed() {
        Intent intent = new Intent(SparkLocationUpdateService.this, WelcomeActivity.class);
        intent.putExtra(NccConstants.IS_REFRESH_TOKEN_FAILED, true);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
    }

    public void mintlogEventExtraData(String event, HashMap<String, String> extraDatas) {

        Mint.clearExtraData();
        if (extraDatas != null) {
            for (Map.Entry<String, String> extraData :
                    extraDatas.entrySet()) {
                Mint.addExtraData(extraData.getKey(), extraData.getValue());
            }
        }

        if (mPrefs != null && !TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""))) {
            Mint.setUserIdentifier(mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
            Mint.addExtraData("user_id", mPrefs.getString(NccConstants.SIGNIN_USER_ID, ""));
        }
        if (mPrefs != null && !TextUtils.isEmpty(mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""))) {
            Mint.addExtraData("facility_id", mPrefs.getString(NccConstants.SIGNIN_VENDOR_ID, ""));
        }

        Mint.logEvent(event, MintLogLevel.Debug);
        Mint.flush();
    }

    public void mintlogEvent(String event) {
        HashMap<String, String> extraData = new HashMap<>();
        if (event.toUpperCase(Locale.ENGLISH).contains("FIREBASE") || event.toUpperCase(Locale.ENGLISH).contains("API")) {
            extraData.put("error", event);
            if (event.toUpperCase(Locale.ENGLISH).contains("FIREBASE")) {
                mintlogEventExtraData("Firebase Exception", extraData);
            } else {
                mintlogEventExtraData("Api Exception", extraData);
            }
        } else {
            mintlogEventExtraData(event, null);
        }

    }


    public static boolean updateJobDataForGeofenceArrayQueue(JobDetail jobDetail) {

        boolean updated = false;
        if (jobDataForGeofenceArrayList != null && !jobDataForGeofenceArrayList.isEmpty()) {
            int i = 0;
            for (JobDetail details : jobDataForGeofenceArrayList) {
                if (jobDetail.getDispatchId().equals(details.getDispatchId())) {
                    jobDataForGeofenceArrayList.set(i, jobDetail);
                    updated = true;
                }
                i++;
            }
        }

        return updated;

    }

    private static boolean removeJobDataForGeofenceArrayQueue(String dispatchid) {

        boolean updated = false;
        if (jobDataForGeofenceArrayList != null && !jobDataForGeofenceArrayList.isEmpty()) {
            int i = 0;
            for (JobDetail details : jobDataForGeofenceArrayList) {
                if (dispatchid.equals(details.getDispatchId())) {
                    jobDataForGeofenceArrayList.remove(i);
                    updated = true;
                    break;
                }
                i++;
            }
        }

        return updated;

    }


    public static void checkJobToAddOrRemove(String userId, JobDetail mCurrentJob) {
        if (mCurrentJob != null && isMyJob(userId, mCurrentJob) && mCurrentJob.getCurrentStatus() != null && (NccConstants.JOB_STATUS_EN_ROUTE.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_ON_SCENE.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) || NccConstants.JOB_STATUS_TOWING.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()))) {
//            if (NccConstants.JOB_STATUS_ON_SCENE.equalsIgnoreCase(mCurrentJob.getCurrentStatus().getStatusCode()) && !Utils.isTow(mCurrentJob.getServiceTypeCode())) {
//                removeJobDataForGeofenceArrayQueue(mCurrentJob.getDispatchId());
//            } else {
            addOrUpdate(mCurrentJob);
//            }
        } else {
            removeJobDataForGeofenceArrayQueue(mCurrentJob.getDispatchId());
        }
    }

    private static void addOrUpdate(JobDetail mCurrentJob) {
        if (!updateJobDataForGeofenceArrayQueue(mCurrentJob)) {
            jobDataForGeofenceArrayList.add(mCurrentJob);
        }
    }

    private static boolean isMyJob(String userId, JobDetail mCurrentJob) {
        boolean isMyJob = false;
        if (mCurrentJob != null && !TextUtils.isEmpty(mCurrentJob.getDispatchAssignedToId())) {
            isMyJob = userId.equalsIgnoreCase(mCurrentJob.getDispatchAssignedToId());
        }
        return isMyJob;
    }

    public static void clearList() {
        jobDataForGeofenceArrayList.clear();
    }


}
